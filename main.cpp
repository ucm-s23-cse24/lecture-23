#include <iostream>
#include <sched.h>
#include <sstream>

using namespace std;


template <class T = int>
class GravityTank{
    T *storage;
    int count;
    int capacity;

public:
    GravityTank(){
        count = 0;
        capacity = 1;
        storage = new T[capacity];
    }

    GravityTank(const GravityTank& other){
        count = other.count;
        capacity = other.capacity;

        storage = new T[capacity];

        for (int i = 0; i < count; i++){
            storage[i] = other.storage[i];
        }
    }

    GravityTank& operator=(const GravityTank& other){

        delete[] storage;

        count = other.count;
        capacity = other.capacity;

        storage = new T[capacity];

        for (int i = 0; i < count; i++){
            storage[i] = other.storage[i];
        }

        return *this;
    }

    void put(T x){
        // Put the element in last position
        storage[count] = x;
        count++;
        // Check if we're out of space
        if (count == capacity){
            capacity *= 2;
            T* temp = new T[capacity];

            for (int i = 0; i < count; i++){
                temp[i] = storage[i];
            }

            T* old = storage;
            storage = temp;
            delete[] old;
        }
        // Check if last inserted item is in the right place
        // and move it forward if necessary
        int j = count-1;
        while (j > 0 && storage[j-1] > storage[j]){
            T temp = storage[j];
            storage[j] = storage[j-1];
            storage[j-1] = temp;
            j--;
        }
    }

    bool operator>(const GravityTank<T>& other){
        return count > other.count;
    }

    bool operator==(const GravityTank<T>& other){
        return count == other.count;
    }

    int size(){
        return count;
    }

    ~GravityTank(){
        delete[] storage;
    }
};


template <class T = int>
ostream& operator<<(ostream& os, const GravityTank<T>& tank){
    for (int i = 0; i < tank.size(); i++){
        os << tank.storage[i];
        if (i < tank.count - 1){
            os << " ";
        }
    }

    return os;
}



int main(int argc, char* argv[]){
    // Your code here

    GravityTank<int> nums;

    nums.put(5);
    nums.put(7);
    nums.put(3);

    GravityTank<> myTank;

    myTank.put(9);
    myTank.put(5);

    // cout << nums << endl;
    // cout << myTank << endl;

    GravityTank<GravityTank<int>> bigBox;

    bigBox.put(nums);
    bigBox.put(myTank);

    cout << bigBox << endl;

	return 0;
}